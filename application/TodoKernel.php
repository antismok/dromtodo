<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Todo\Engine\Components\Request;
use Symfony\Component\Yaml\Yaml;
use Todo\Engine\Components\Router;
use Todo\Engine\Components\Di\TodoContainer;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

/**
 * Description of TodoKernel
 *
 * @author ak
 */
class TodoKernel {
    
    protected $rootDir;
    protected $container;
    protected $isBooted = false;
    protected $host = '';
    
    public function __construct(){
        $this->boot();
    }

    public function handleRequest(Request $request)
    {
        $this->boot();
        $this->setHost($request->getBaseUrl());//TODO Костыль. пофиксить
        echo $this->container->offsetGet('router')->resolveController($request);
    }
    
    protected function boot()
    {
        if (!$this->isBooted) {
            $this->initContainer();
            $this->isBooted = true;
        }
    }
    
    public function getHost() 
    {
        return $this->host;
    }
    
    protected function setHost($host) 
    {
        $this->host = $host;
    }
    
    public function getRootDir()
    {
        if (null === $this->rootDir) {
            $r = new \ReflectionObject($this);
            $this->rootDir = dirname($r->getFileName());
        }

        return $this->rootDir;
    }
    
    public function getProjectDir()
    {
        return $this->getRootDir().'/../src';
    }
    
    public function getCacheDir()
    {
        return $this->getRootDir().'/cache';
    }
    
    protected function initContainer()
    {
        $this->container = new TodoContainer();
        $this->container['kernel'] = function ($c) {
            return $this;
        };
        $this->container['yaml_parser'] = function ($c) {
            return new Yaml();
        };
        
        $this->container['router'] = function ($c) {
            return new Router($c,$c['yaml_parser'],$c['kernel']->getRootDir().'/routing.yml');
        };

        $this->container['twig'] = function ($c) {
            $tamplatePaths = [
                $c['kernel']->getProjectDir().'/TodoList/Resources/view',
                $c['kernel']->getProjectDir().'/Users/Resources/view',
                $c['kernel']->getRootDir().'/Resources/view',
                $c['kernel']->getRootDir().'/../vendor/Todo/Engine/Components/Form/Widgets/view',
            ];
            $loader = new \Twig_Loader_Filesystem($tamplatePaths);
            return new \Twig_Environment($loader, array(
                'cache' => $c['kernel']->getCacheDir(),
            ));
        };
        
        $this->container['doctrine'] = function ($c) {
            $paramsRaw = file_get_contents($c['kernel']->getRootDir().'/parameters.yml');
            $params = $c['yaml_parser']->parse($paramsRaw);
            $dbParams['driver']     = $params['parameters']['database_driver'];
            $dbParams['host']       = $params['parameters']['database_host'];
            $dbParams['port']       = $params['parameters']['database_port'];
            $dbParams['dbname']     = $params['parameters']['database_name'];
            $dbParams['user']       = $params['parameters']['database_user'];
            $dbParams['password']   = $params['parameters']['database_password'];

            $isDevMode = false;
            $config = Setup::createAnnotationMetadataConfiguration([
                    $c['kernel']->getProjectDir().'/TodoList/Entity/',
                    $c['kernel']->getProjectDir().'/Users/Entity/',
                ], 
                $isDevMode
            );
            return EntityManager::create($dbParams, $config);
        };
    }
    
    public function getContainer()
    {
        return $this->container;
    }
}
