<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/**
 * @var Composer\Autoload\ClassLoader
 */
$loader = require __DIR__.'/application/autoload.php';
require_once __DIR__.'/application/TodoKernel.php';

$kernel = new TodoKernel();
$em = $kernel->getContainer()->get('doctrine');

$helperSet = new \Symfony\Component\Console\Helper\HelperSet([
    'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($em->getConnection()),
    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em)
]);

$commands = [];
\Doctrine\ORM\Tools\Console\ConsoleRunner::run($helperSet, $commands);
