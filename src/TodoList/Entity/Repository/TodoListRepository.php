<?php

namespace TodoList\Entity\Repository;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Doctrine\ORM\EntityRepository;

/**
 * Description of TodoListRepository
 *
 * @author saritskiy.r@gmail.com
 */
class TodoListRepository extends EntityRepository
{
    /**
     * Возвращает задачи для текущего пользователя
     * @param integer   $userId
     * @param mixed     $isClosed
     * @return doctrine ArrayCollection
     */
    public function getMyTodos($userId,$isClosed = '') {
        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('tl')
            ->from('TodoList\Entity\TodoList', 'tl')
            ->where('tl.userId =:userId')
            ->setParameter('userId', $userId)
        ;
        if ($isClosed !== '') {
            $query
                ->andWhere('tl.isClosed = :isClosed')
                ->setParameter('isClosed',  boolval($isClosed))
            ;
        }
        return $query->orderBy("tl.id", "DESC")->getQuery()->getResult();
    }
}
