<?php
namespace TodoList\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Users\Entity\User;

/**
 * @Entity(repositoryClass="TodoList\Entity\Repository\TodoListRepository")
 * @Table(name="todo_list")
 **/
class TodoList {

    /** 
     * @Id @Column(type="integer") 
     * @GeneratedValue 
     **/
    protected $id;
    
    /**
     * @var integer
     *
     * @Column(name="user_id", type="integer", nullable=false)
     */
    protected $userId;
    
    /**
     * @ManyToOne(targetEntity="Users\Entity\User", inversedBy="tasks")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /** 
     * @Column(type="string") 
     **/
    protected $task;
    
    /**
     * @Column(name="is_closed", type="boolean") 
     */
    protected $isClosed = false;


    public function getId() {
        return $this->id;
    }

    public function getTask() {
        return $this->task;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setTask($task) {
        $this->task = $task;
    }

    public function getUser() {
        return $this->user;
    }

    public  function setUser($user) {
        $this->user = $user;
        $this->setUserId($user->getId());
    }

    public function getUserId() {
        return $this->userId;
    }

    public function setUserId($userId) {
        $this->userId = $userId;
    }
    function getIsClosed() {
        return $this->isClosed;
    }

    function setIsClosed($isClosed) {
        $this->isClosed = $isClosed;
    }
}
