<?php

namespace TodoList\Form;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Todo\Engine\Components\Form\AbstractForm;
use Todo\Engine\Components\Form\Widgets\InputWidget;
use Todo\Engine\Components\Form\Validators\PassValidator;
use Todo\Engine\Components\Form\Validators\IntegerValidator;

/**
 * Description of TodoForm
 *
 * @author ak
 */
class TodoForm extends AbstractForm
{
    public function __construct($templateHandler)
    {
        $this
            ->setTemplateHandler($templateHandler)
            ->setName('todo')
            ->add(
                'task',
                InputWidget::class,
                PassValidator::class,
                []
            )
        ;
        
    }
}
