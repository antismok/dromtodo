<?php

namespace TodoList\Controller;

use Todo\Engine\Components\Controller\BaseController;
use Todo\Engine\Components\Request;
use Todo\Engine\Components\Response;
use TodoList\Form\TodoForm;
use TodoList\Entity\TodoList;

/**
 * Description of TodoListController
 *
 * @author saritskiy.r@gmail.com
 */
class TodoListController extends BaseController
{
    /**
     * General page
     * 
     * @param Request $request
     * @return string Page content
     */
    public function index(Request $request)
    {
        $this->redirectUnless($this->getAuthenticated(), "user_login");
        $em = $this->getEntityManager();
        $form = new TodoForm($this->container->get('twig'));
        $todos = $em->getRepository("TodoList\Entity\TodoList")->getMyTodos($this->getUser()->getId());
        return $this->renderView('index.html.twig',[ 
            'todos' => $todos, 
            'form'  => $form, 
            'user'  => $this->getUser()->getLogin()
        ]);
    }
    
    /**
     * Adding new todo
     * 
     * @param Request $request
     * @return string Json encoded array
     */
    public function newTodo(Request $request)
    {
        $this->redirectUnless($this->getAuthenticated(), "user_login");
        $form = new TodoForm($this->container->get('twig'));
        $task = $request->request->get("task");
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getEntityManager();
            $todo = new TodoList();
            $todo->setTask($task);
            $todo->setUser($this->getUser());
            $task = $em->merge($todo);
            $em->flush();

            $content = $this->renderView('new.html.twig',[ 'task' => $task]);
            
            return json_encode([
                "state"     => 'success',
                'content'   => $content
            ]);
            
        }
        return json_encode([
            "state"     => 'fail',
            'errors'    => 'error'
        ]);
    }
    
    /**
     * Delete todo
     * 
     * @param Request $request
     * @return string Json encoded array
     */
    public function deleteTodo(Request $request)
    {
        //TODO: метод DELETE
        $this->redirectUnless($this->getAuthenticated(), "user_login");
        $em = $this->getEntityManager();
        $id = $request->request->get('id',false);
        if ($id && $todo = $em->getRepository("TodoList\Entity\TodoList")->find($id)) {
            $this->isGranded($todo);
            $em = $this->getEntityManager();
            $em->remove($todo);
            $em->flush();

            return json_encode(['state' => "success"]);
        }
        
        return json_encode([
            'state' => "fail",
            'error' => 'id is wrong'
        ]);
    }
    
    /**
     * Set/unset closed for todo
     * 
     * @param Request $request
     * @return string Json encoded array
     */
    public function changeStateTodo(Request $request)
    {
        $this->redirectUnless($this->getAuthenticated(), "user_login");
        $em = $this->getEntityManager();
        $id = $request->request->get('id',false);
        if ($id && $todo = $em->getRepository("TodoList\Entity\TodoList")->find($id)) {
            $this->isGranded($todo);
            $close = $request->request->get('close',false);
            $em = $this->getEntityManager();
            $todo->setIsClosed($close);
            $em->merge($todo);
            $em->flush();

            return json_encode(['state' => "success"]);
        }
        
        return json_encode([
            'state' => "fail",
            'error' => 'id is wrong'
        ]);
    }
    
    /**
     * Ajax get list of all/closed/actual todos
     * 
     * @param Request $request
     * @return string Json encoded array
     */
    public function getTodo(Request $request)
    {
        $this->redirectUnless($this->getAuthenticated(), "user_login");
        $em = $this->getEntityManager();
        $isClosed = $request->request->get('active','');
        $form = new TodoForm($this->container->get('twig'));
        $todos = $em->getRepository("TodoList\Entity\TodoList")->getMyTodos($this->getUser()->getId(),$isClosed);
        $response = '';
        foreach ($todos as $task) {
            $response .= $this->renderView('new.html.twig',[ 'task' => $task]);
        }
        return json_encode([
            "state"     => 'success',
            'content'   => $response
        ]);
    }
    
}
