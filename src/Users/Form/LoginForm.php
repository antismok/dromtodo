<?php

namespace Users\Form;

use Todo\Engine\Components\Form\AbstractForm;
use Todo\Engine\Components\Form\Widgets\InputWidget;
use Todo\Engine\Components\Form\Validators\PassValidator;
use Todo\Engine\Components\Form\Validators\IntegerValidator;
use Todo\Engine\Components\Form\Validators\NotNullValidator;

/**
 * Description of LoginForm
 *
 * @author ak
 */
class LoginForm extends AbstractForm
{
    public function __construct($templateHandler)
    {
        $this
            ->setTemplateHandler($templateHandler)
            ->setName('auth')
            ->add(
                'login', 
                InputWidget::class,
                NotNullValidator::class,
                [
                    'attributes' => [
                        'class' => 'form-control',
                        'style' => 'width:150px;'
                        
                    ]
                ])
            ->add(
                'password', 
                InputWidget::class,
                NotNullValidator::class,
                [
                    'attributes' => [
                        'class' => 'form-control',
                        'style' => 'width:150px;',
                        'type'  => 'password'
                    ]
                ])
        ;
        
    }
}
