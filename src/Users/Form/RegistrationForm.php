<?php

namespace Users\Form;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Todo\Engine\Components\Form\AbstractForm;
use Todo\Engine\Components\Form\Widgets\InputWidget;
use Todo\Engine\Components\Form\Validators\PassValidator;
use Todo\Engine\Components\Form\Validators\IntegerValidator;
use Todo\Engine\Components\Form\Validators\NotNullValidator;



/**
 * Description of RegistrationForm
 *
 * @author ak
 */
class RegistrationForm extends AbstractForm 
{

    public function __construct($templateHandler)
    {
        $this
            ->setTemplateHandler($templateHandler)
            ->setName('registration')
            ->add(
                'login', 
                InputWidget::class,
                NotNullValidator::class,
                [
                    'attributes' => [
                        'class' => 'form-control',
                        'style' => 'width:150px;'
                    ]
                ])
            ->add(
                'password', 
                InputWidget::class,
                NotNullValidator::class,
                [
                    'attributes' => [
                        'class' => 'form-control',
                        'style' => 'width:150px;',
                        'type'  => 'password'
                    ]
                ])
             ->add(
                'password_confirm',
                InputWidget::class,
                PassValidator::class,
                [
                    'attributes' => [
                        'class' => 'form-control',
                        'style' => 'width:150px;',
                        'type'  => 'password'
                    ]
                ])
        ;
        
    }
}
