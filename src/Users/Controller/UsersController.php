<?php

namespace Users\Controller; 

use Todo\Engine\Components\Controller\BaseController;
use Todo\Engine\Components\Request;
use Todo\Engine\Components\Response;
use Users\Form\RegistrationForm;
use Users\Form\LoginForm;
use Users\Entity\User;

/**
 * Description of UserController
 *
 * @author ak
 */
class UsersController extends BaseController
{
    /**
     * User registration method
     * 
     * @param Request $request
     * @return string
     */
    public function registerUser(Request $request) 
    {
        $this->redirectUnless(!$this->getAuthenticated(), "default");
        $form = new RegistrationForm($this->container->get('twig'));
        if ($request->getMethod() !== "POST") {
            return $this->renderView('registration.html.twig',[ 'form' => $form ]);
        }

        $form->bind($request);
        if ($form->isValid()) {
            $em = $this->getEntityManager();
            //TODO: перетащить такие проверки в поствалидатор
            $users = $em->getRepository("Users\Entity\User")->findBy(['login' => $request->request->get('login')]);
            if ($users) {
               $form->setError('login','Пользователь с таким логином уже существует');
               return $this->renderView('registration.html.twig',[ 'form' => $form ]);
            }
            if ($request->request->get('password') !== $request->request->get('password_confirm')){
               $form->setError('password','Пароли не совпадают');
               return $this->renderView('registration.html.twig',[ 'form' => $form ]);
            }
            //End Todo
            $passwordHash = password_hash($request->request->get('password'), PASSWORD_BCRYPT, ['cost' => 11]);
            $user = new User();
            $user->setLogin($request->request->get('login'));
            $user->setPassword($passwordHash);
            $em->persist($user);
            $em->flush();
            $this->setUser($user);
        }
        return $this->renderView('registration.html.twig',[ 'form' => $form ]);
    }
    
    /**
     * User Authentication method
     * 
     * @param Request $request
     * @return string
     */
    public function loginUser(Request $request)
    {
        $this->redirectUnless(!$this->getAuthenticated(), "default");
        $form = new LoginForm($this->container->get('twig'));
        if ($request->getMethod() !== "POST") {
           return $this->renderView('login.html.twig',[ 'form' => $form ]); 
        }

        $form->bind($request);
        if ($form->isValid()) {
            $em = $this->getEntityManager();
            $user = $em->getRepository("Users\Entity\User")->findOneBy(['login' => $request->request->get('login')]);
            if (!$user || !password_verify($request->request->get('password'),$user->getPassword())) {
                $form->setError('login','Такого соотношения логин/пароль не существует');
                return $this->renderView('login.html.twig',[ 'form' => $form ]);
            }
            $this->setUser($user);
            return $this->redirect("default");
        }
        return $this->renderView('login.html.twig',[ 'form' => $form ]);
    }
    
    /**
     * User logout method
     * 
     * @param Request $request
     */
    public function logoutUser(Request $request)
    {
        $this->unsetUser();
        $this->redirect('user_login');
    }
}
