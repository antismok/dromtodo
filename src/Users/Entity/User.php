<?php
namespace Users\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use TodoList\Entity\TodoList;

/**
 * @Entity(repositoryClass="Users\Entity\Repository\UserRepository")
 * @Table(name="user")
 **/
class User {
    
    public function __construct() {
        $this->tasks = new ArrayCollection();
    }

    /** 
     * @Id @Column(type="integer") 
     * @GeneratedValue 
     **/
    protected $id;
    
    /**
     * @OneToMany(targetEntity="TodoList\Entity\TodoList", mappedBy="user")
     */
    protected $tasks;

    /** 
     * @Column(type="string", unique = true) 
     **/
    protected $login;
    
    /** 
     * @Column(type="string") 
     **/
    protected $password;
    
    public function getId() {
        return $this->id;
    }

    public function getLogin() {
        return $this->login;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getSalt() {
        return $this->salt;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setLogin($login) {
        $this->login = $login;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function setSalt($salt) {
        $this->salt = $salt;
    }
    public function getTasks() {
        return $this->tasks;
    }

    public function setTasks($tasks) {
        $this->tasks = $tasks;
    }
}
