(function() {
    var list = {
        sendNewTask: function(obj) {
            self = this;
            if (!obj.val() || 0 === obj.val().length) { return; }
            var url = obj.context.baseURI + "todo/new";
            //TODO: вынести все запросы в отдельным метод и дергать его
            $.post(url, {task: obj.val()})
                .done(function( data ) {
                    data = jQuery.parseJSON(data);
                    if (data.state === "success") {
                        $("#todo-list").prepend(data.content);
                        self.recalculate();
                    }
                })
            ;
        },
        getTasks: function(obj) {
            self    = this;
            uri     = obj.context.baseURI;
            url     = uri + '/todos';
            $.post(url, {active: obj.data('active')})
                .done(function( data ) {
                    data = jQuery.parseJSON(data);
                    if (data.state === "success") {
                        $("#todo-list").empty().prepend(data.content);
                        self.recalculate();
                    }
                })
            ;
        },
        changeTaskState: function(obj) {
            self = this;
            var url = obj.context.baseURI + "todo/changestate";
            data = {
                close: obj.prop("checked") ? 1 : 0,
                id: obj.data('id')
            };
            $.post(url, data)
                .done(function( data ) {
                    data = jQuery.parseJSON(data);
                    if(data.state === "success") {
                        obj.parent().parent().hasClass('completed') 
                            ? obj.parent().parent().removeClass('completed') 
                            : obj.parent().parent().addClass('completed')
                        ;
                        self.recalculate();
                    }
                })
            ;
        },
        deleteTask: function(obj) {
            self = this;
            var url = obj.context.baseURI + "todo/delete";
            $.post(url, {id: obj.data('id')})
                .done(function( data ) {
                    data = jQuery.parseJSON(data);
                    obj.parent().remove();
                    self.recalculate();
                })
            ;
        },
        recalculate: function(){
            //TODO: Запихать это в куки и вынести это быдлячество к чертям
            //Чтобы на любой вкладке считалось общее кол-во, а не только одного статуса
            //и не вешать на каждый метод,а воткнуть иветн лиснер
            var sum = 0;
            $("input[type=checkbox]:not(:checked)").each(function(){
                sum++;
            });
            //TODO: И не полениться добавить айдишник чтобы не пихать строку!!!
            $("#todo-count").html("<strong>"+sum+"<\strong> items left");
        }
    };

    window.onload = function() {
        $("#new-todo").on( "keydown",function(e) {
            if(e.which === 13) {
               list.sendNewTask($(this));
            }
        });

        $('#todo-list').on('click','.destroy',function(){
            list.deleteTask($(this));
        });

        $('#todo-list').on('click','.toggle',function(){
            list.changeTaskState($(this));
        });
        
        $('.filter_button').on('click',function(){
            $('.filter_button').removeClass('selected');
            $(this).addClass('selected');
            list.getTasks($(this));
            return false;
        });
    };

})();


