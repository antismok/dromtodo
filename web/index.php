<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

use Todo\Engine\Components\Request;

/**
 * @var Composer\Autoload\ClassLoader
 */
$loader = require __DIR__.'/../application/autoload.php';

require_once __DIR__.'/../application/TodoKernel.php';
$kernel = new TodoKernel();
$request = Request::createFromGlobals();
$kernel->handleRequest($request);
