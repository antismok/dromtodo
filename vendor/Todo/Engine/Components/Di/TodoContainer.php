<?php

namespace Todo\Engine\Components\Di;

use Todo\Engine\Components\Di\DiContainerInterface;
use Pimple\Container;

/**
 * Description of TodoContainer
 *
 * @author ak
 */
class TodoContainer extends Container implements DiContainerInterface {

    public function get($key) {
        return $this->offsetGet($key);
    }

    public function set($key) {
        return $this->offsetSet($key);
    }
    
    public function has($key) {
        return $this->offsetExists($key);
    }
}
