<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Todo\Engine\Components\Di;

/**
 * Description of ContainerAwareTrait
 *
 * @author ak
 */
trait ContainerAwareTrait {
    /**
     * @var DiContainerInterface
     */
    protected $container;

    /**
     * Sets the container.
     *
     * @param DiContainerInterface|null $container A DiContainerInterface instance or null
     */
    public function setContainer(DiContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
