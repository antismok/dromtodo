<?php

namespace Todo\Engine\Components\Di;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DiContainerInterface
 *
 * @author ak
 */
interface  DiContainerInterface {
    public function get($key);
    public function set($key);
}
