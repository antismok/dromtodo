<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Todo\Engine\Components\Di;

/**
 * Description of ContainerAwareIntarface
 *
 * @author ak
 */
interface ContainerAwareIntarface {
    
    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(DiContainerInterface $container = null);
}
