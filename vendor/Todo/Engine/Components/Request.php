<?php

namespace Todo\Engine\Components;

use Todo\Engine\Components\ParametersWrapper;
use Todo\Engine\Components\ServerWrapper;

/**
 * Description of Request
 *
 * @author saritskiy roman 
 */
class Request 
{
    /**
     * Request body parameters ($_POST).
     *
     * @var \Todo\Engine\Components\ParametersWrapper
     */
    public $request;

    /**
     * Query string parameters ($_GET).
     *
     * @var \Todo\Engine\Components\ParametersWrapper
     */
    public $query;

    /**
     * Server and execution environment parameters ($_SERVER).
     *
     * @var \Todo\Engine\Components\ServerWrapper
     */
    public $server;
    
    /**
     * Cookies ($_COOKIE).
     *
     * @var \Todo\Engine\Components\ParametersWrapper
     */
    public $cookies;
    
    /**
     * @var string
     */
    protected $content;
    
    /**
     * @var string
     */
    protected $pathInfo;

    /**
     * @var string
     */
    protected $requestUri;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var string
     */
    protected $basePath;

    /**
     * @var string
     */
    protected $method;

    /**
     * @var string
     */
    protected $format;

    /**
     * @var \Todo\Engine\Components\SessionInterface
     */
    protected $session;
   
    /**
     * Constructor.
     *
     * @param array           $query      The GET parameters
     * @param array           $request    The POST parameters
     * @param array           $attributes The request attributes (parameters parsed from the PATH_INFO, ...)
     * @param array           $cookies    The COOKIE parameters
     * @param array           $server     The SERVER parameters
     * @param string|resource $content    The raw body data
     */
    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $server = array(), $content = null)
    {
        // Добавим возможность перинициализации при необходимости, хотя в данном примере это врятли понадобится
        $this->initialize($query, $request, $attributes, $cookies, $server, $content);
    }

    /**
     * Initialaze variables
     *
     * @param array           $query      The GET parameters
     * @param array           $request    The POST parameters
     * @param array           $attributes The request attributes (parameters parsed from the PATH_INFO, ...)
     * @param array           $cookies    The COOKIE parameters
     * @param array           $files      The FILES parameters
     * @param array           $server     The SERVER parameters
     * @param string|resource $content    The raw body data
     */
    public function initialize(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null)
    {
        $this->request = new ParametersWrapper($request);
        $this->query = new ParametersWrapper($query);
        $this->attributes = new ParametersWrapper($attributes);
        $this->cookies = new ParametersWrapper($cookies);
        $this->server = new ServerWrapper($server);
        $this->headers = new HeaderWrapper($this->server->getHeaders());

        $this->content = $content;
        $this->languages = null;
        $this->charsets = null;
        $this->encodings = null;
        $this->acceptableContentTypes = null;
        $this->pathInfo = $this->server->get("PATH_INFO");
        $this->requestUri = null;
        //TODO: Пофиксить этот костыль на скорую руку
        $this->baseUrl = $this->server->has("HTTPS") ? "https://".$this->server->get("HTTP_HOST") : "http://".$this->server->get("HTTP_HOST");
        $this->basePath = null;
        $this->method = null;
        $this->format = null;
    }

    /**
     * Wrapp from global php variable
     *
     * @return \Todo\Engine\Components\Request
     */
    public static function createFromGlobals()
    {
        $server = $_SERVER;
        if ('cli-server' === \php_sapi_name()) {
            if (\array_key_exists('HTTP_CONTENT_LENGTH', $_SERVER)) {
                $server['CONTENT_LENGTH'] = $_SERVER['HTTP_CONTENT_LENGTH'];
            }
            if (\array_key_exists('HTTP_CONTENT_TYPE', $_SERVER)) {
                $server['CONTENT_TYPE'] = $_SERVER['HTTP_CONTENT_TYPE'];
            }
        }

        $request = new static($_GET, $_POST, array(), $_COOKIE, [],$server);

        if (0 === \strpos($request->headers->get('CONTENT_TYPE'), 'application/x-www-form-urlencoded')
            && \in_array(\strtoupper($request->server->get('REQUEST_METHOD', 'GET')), array('PUT', 'DELETE', 'PATCH'))
        ) {
            $data = [];
            \parse_str($request->getContent(), $data);
            $request->request = new ParameterWrapper($data);
        }

        return $request;
    }
    /**
     * Get cuttent request base url 
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }
    
    /**
     * Return current reques method
     * @return string
     */
    public function getMethod()
    {
        return $this->server->get('REQUEST_METHOD', 'GET');
    }
}
