<?php

namespace Todo\Engine\Components\Controller;

use Todo\Engine\Components\Di\ContainerAwareIntarface;
use Todo\Engine\Components\Di\ContainerAwareTrait;

/**
 * Description of BaseController
 *
 * @author ak
 */
abstract class BaseController implements ContainerAwareIntarface
{
    use ContainerAwareTrait;
    
    /**
     * Returns a rendered view.
     *
     * @param string $view       The view name
     * @param array  $parameters An array of parameters to pass to the view
     *
     * @return string The rendered view
     */
    protected function renderView($view, array $parameters = array())
    {
        if (!$this->container->has('twig')) {
            throw new \LogicException('You can not use the "renderView" method if the Templating Component or the Twig are not available.');
        }

        return $this->container->get('twig')->render($view, $parameters);
    }
    
    /**
     * Shortcut to return the Doctrine Registry service.
     *
     * @return Registry
     *
     * @throws \LogicException If DoctrineBundle is not available
     */
    protected function getEntityManager()
    {
        if (!$this->container->has('doctrine')) {
            throw new \LogicException('The DoctrineBundle is not registered in your application.');
        }

        return $this->container->get('doctrine');
    }
    
    /**
     * Redirect to 403 if is not your entity
     * 
     * @param DoctrineEntity $entity
     */
    protected function isGranded($entity) {
        //TODO: ПО сути нужен отдельный класс проверки для разных сущностей
        //Либо интерфейс для сущности пилить
        if ($entity->getUserId() !== $this->getUser()->getId()) {
            echo "403 Forbidden";
            header('HTTP/1.1 403 Forbidden');
        }
    }
    
    /**
     * Return current user
     * 
     * @return Users\Entity\User|false
     */
    protected function getUser()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        return isset($_SESSION['user']) ? $_SESSION['user'] : false;
    }
    
    /**
     * Clear the current user and close session
     */
    protected function unsetUser()
    {
        //TODO: Вынести работу с сессиями в правильное место
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if (isset($_SESSION['user'])) {
            unset($_SESSION['user']);
        }
        if (session_status() != PHP_SESSION_NONE) {
            session_destroy();
        }
    }
    
    /**
     * Start session and set user
     * @param Users\Entity\User $user
     */
    protected function setUser($user)
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if (!isset($_SESSION['user'])) {
            $_SESSION['user'] = $user;
        }
    }

    /**
     * Check on user is authenticated
     * 
     * @return boolean
     */
    public function getAuthenticated() 
    {
        return $this->getUser() ? true : false;
    }
    
    /**
     * Redirect to route
     * 
     * @param string    $href   name of route       
     * @param integer   $status redirect code
     * 
     * @throws \LogicException
     */
    public function redirect($href, $status = 302) {
        if (!$this->container->has('router')) {
            throw new \LogicException('The DoctrineBundle is not registered in your application.');
        }
        $link = $this->container->get('router')->getRoute($href);
        $this->container->get('router')->redirect($link);
    }
    
    /**
     * Redirect to route is $condition is false
     * 
     * @param boolean    $condition     condition      
     * @param string     $link          name of route 
     * 
     * @throws \LogicException
     */
    public function redirectUnless($condition,$link) 
    {
        if (!$condition) {
            $this->redirect($link);
        }
    }
}
