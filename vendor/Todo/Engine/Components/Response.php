<?php

namespace Todo\Engine\Components;

use Todo\Engine\Components\HeaderWrapper;

/**
 * Description of Response
 *
 * @author ak
 */
class Response 
{
    protected  $content = '';
    protected  $status  = 200;
    protected  $protocolVersion = '1.0';

    public function __construct($content = '', $status = 200, $headers = [])
    {
        $this->setContent($content);
        $this->setStatusCode($status);
    }
    
    public function setContent($content) 
    {
        $this->content = $content;
    }
    
    public function setStatusCode($status)
    {
        $this->status = $status;
    }
    
    public static function create($content = '', $status = 200, $headers = array())
    {
        return new static($content, $status, $headers);
    }
    
    public function prepare(Request $request)
    {
        // Todo: обработка и обертка заголовков
        
        $this->protocolVersion = $request->server->get('SERVER_PROTOCOL');
    }
    
    /**
     * Sends HTTP headers.
     *
     * @return Response
     */
    public function sendHeaders()
    {
        if (headers_sent()) {
            return $this;
        }

        //Обработка загловков 

        header(sprintf('HTTP/%s %s %s', $this->protocolVersion, $this->status, $this->status), true, $this->status);

       //TODO: Добавить куки

        return $this;
    }
    
    /**
     * Sends content for the current web response.
     *
     * @return Response
     */
    public function sendContent()
    {
        echo $this->content;

        return $this;
    }

    /**
     * Sends HTTP headers and content.
     *
     * @return Response
     */
    public function send()
    {
        $this->sendHeaders();
        $this->sendContent();

        return $this;
    }
}
