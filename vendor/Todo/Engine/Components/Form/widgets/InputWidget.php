<?php

namespace Todo\Engine\Components\Form\Widgets;

/*
 * Input widget
 */

use Todo\Engine\Components\Form\Widgets\AbstractWidget;

/**
 * Description of InputWidget
 *
 * @author ak
 */
class InputWidget extends AbstractWidget
{
    public function __construct() 
    {
        parent::__construct(func_get_args);
    }
    
    /**
     * Возвращает отрисованный виджет
     * 
     * @return string
     * @throws Exception
     */
    public function render() 
    {
        if (!$this->templateHandler || !$this->template) {
            throw new Exception("Template handler or template not set");
        }

        return $this->templateHandler->render(
            $view, [
                'name' => $this->name,
                'value' => $this->value,
                'options' => $this->options,
                'attributes' => $this->attributes,
                'id' => $this->id
            ]
        );
    }
    
    /**
     * Устанавливает имя шаблона для виджета
     * @param string $template
     */
    public function setTemplate($template = null) {
        $template !== null ? $this->template = $template : $this->template = "base_input_widget.html.twig";
    }
}
