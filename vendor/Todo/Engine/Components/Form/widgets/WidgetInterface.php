<?php

namespace Todo\Engine\Components\Form\Widgets;
/*
 * Интерфейс который должен реализовывать любой виджет
 */

/**
 * Description of WidgetInterface
 *
 * @author ak
 */
interface WidgetInterface {
    public function setTemplateHandler($templateHandler);
    public function setAttibutes($attributes);
    public function setOptions($options);
    public function setValue($value);
    public function setName($name);
    public function setId($name);
    public function render();
}
