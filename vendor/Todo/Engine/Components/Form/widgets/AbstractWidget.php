<?php

/*
 *  А вот собственно и пример использования полиморфизма
 */

namespace Todo\Engine\Components\Form\Widgets;

use Todo\Engine\Components\Form\Widgets\WidgetInterface;

/**
 * Description of AbstractWidget
 *
 * @author ak
 */
abstract class AbstractWidget implements WidgetInterface
{
    private $attributes = [];
    private $options = [];
    private $value = [];
    private $name;
    private $templateHandler;
    private $id;
    private $template;
    
    public function __construct(string $name, $attributes, $options, $id = null, $templateHandler = null, $value = null) {
        $this->setTemplateHandler($templateHandler);
        $this->setAttibutes($attributes);
        $this->setOptions($options);
        $this->setValue($value);
        $this->setName($name);
        $this->setId($id);
    }
    
    public function setTemplateHandler($templateHandler)
    {
        $this->templateHandler = $templateHandler;
    }
    
    public function setAttibutes($attributes)
    {
        $this->attributes = $attributes;
    }
    
    public function setOptions($options)
    {
        $this->options = $options;
    }
    
    public function setValue($value)
    {
        $this->value = $value;
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }

    abstract public function render();
    abstract public function setTemplate($template);
    
    public function __toString() {
        return $this->render();
    }
}
