<?php

namespace Todo\Engine\Components\Form;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Todo\Engine\Components\Form\Widgets\WidgetInterface;
use Todo\Engine\Components\Form\Widgets\InputWidget as InputWidget;
use Todo\Engine\Components\Form\Validators\PassValidator;
use Todo\Engine\Components\Request;

/**
 * Description of BaseForm
 *
 * @author ak
 */
abstract class AbstractForm implements \ArrayAccess 
{
    
    public $fields;
    public $name;
    public $templateHandler;
    
    public function setName(string $name) 
    {
        $this->name = $name;
        return $this;
    }

    public function add(string $name, string $widget, string $validator, array $options = [])
    {
        if (!$this->name) {
            throw new Exception("Form name not set");
        }
       
        $this->fields[$name]['validator'] = new $validator();
        $widgetId = $this->name."_".$name;
        $default = ['attributes' => [],'options' => []];
        $paramsArray = array_replace($default, $options);
        $this->fields[$name]['widget'] = new $widget($name, $paramsArray['attributes'], $paramsArray['options'],$widgetId);
        $this->fields[$name]['widget']->setTemplateHandler($this->templateHandler);
        return $this;
    }
    
    public function setTemplateHandler($templateHandler) {
        $this->templateHandler = $templateHandler;
        return $this;
    }
    
    public function offsetExists($name) 
    {
        return isset($this->fields[$name]);
    }

    /**
     * Return the widget array
     *  
     * @param string $name
     * @return array|null
     */
    public function offsetGet($name)
    {
       return isset($this->fields[$name]) ? $this->fields[$name]['widget'] : null;
    }
         
    /**
     * Return the widget
     * 
     * @param string $name  Name of widget
     * @return Todo\Engine\Components\Form\Widgets\WidgetInterface|null
     */
    public function getWidget($name) {
        return $this->offsetGet($name);
    }
    
    /**
     * Return the validator of widget
     * 
     * @param string $name  Name of widget
     * @return Todo\Engine\Components\Form\Validators\ValidatorInterface|null
     */
    public function getValidator($name) {
        return isset($this->fields[$name]) ? $this->fields[$name]['validator'] : null;
    }
    
    public function offsetSet($name,$value ){}

    /**
     * Remove the widget
     * 
     * @param string $name
     */
    public function offsetUnset($name)
    {
        if (isset($this->fields[$name])) {
            unset($this->fields[$name]);
        }
    }

    public function __toString() {
        $data = '';
        foreach ($this->fields as $name => $widget) {
            $data .= $this->getWidget($name);
        }
        return $data;
    }
    
    /**
     * Check form wodgets on conformity types
     */
    private function validate() {
        $this->errors = [];
        foreach ($this->fields as $name => $data) {
            $widget = $this->getWidget($name);
            $validator = $this->getValidator($name);
            if (!$validator->validate($widget->getValue())) {
                $this->errors[$name] = $validator->getError();
                $widget->setError($validator->getError());
            }
        }
    }
    
    /**
     * Check form on errors
     * 
     * @return boolean
     */
    public function isValid() {
        $this->validate();
        return empty($this->errors);
    }
    
    /**
     * Return errors of form
     * @return array
     */
    public function getErrors() {
        if (empty($this->errors)){
            $this->validate();
        }
        return $this->errors;
    }

    /**
     * Mapping request values on form
     * 
     * @param Request $request
     */
    public function bind(Request $request) {
        foreach ($this->fields as $name => $data) {
            $value = $request->query->get($name);
            if (null === $value) {
                $value = $request->request->get($name);
            }
            $widget = $this->getWidget($name);
            if ($widget === null) { continue; }
            $widget->setValue($value);
        }
    }
    
    /**
     * Set error to widget
     * @param string $name  Field name
     * @param string $error Error description
     */
    public function setError($name,$error) 
    {
        $this
            ->getWidget($name)
            ->setError($error)
        ;
    }
}
