<?php

/*
 *  А вот собственно и пример использования полиморфизма
 */

namespace Todo\Engine\Components\Form\Widgets;

use Todo\Engine\Components\Form\Widgets\WidgetInterface;

/**
 * Description of AbstractWidget
 *
 * @author ak
 */
abstract class AbstractWidget implements WidgetInterface
{
    protected $attributes = [];
    protected $error = '';
    protected $options = [];
    protected $value = [];
    protected $name;
    protected $templateHandler;
    protected $id;
    protected $template;
    
    public function __construct(string $name, $attributes, $options, $id = null, $templateHandler = null, $value = null) {
        $this->setTemplateHandler($templateHandler);
        $this->setAttibutes($attributes);
        $this->setOptions($options);
        $this->setValue($value);
        $this->setName($name);
        $this->setId($id);
    }
    
    public function setTemplateHandler($templateHandler)
    {
        $this->templateHandler = $templateHandler;
    }
    
    public function setAttibutes($attributes)
    {
        $this->attributes = $attributes;
    }
    
    public function setOptions($options)
    {
        $this->options = $options;
    }
    
    public function setValue($value)
    {
        $this->value = $value;
    }
    
    public function getValue() {
        return $this->value;
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }

    abstract public function render();
    abstract public function setTemplate($template);
    
    public function __toString() {
        return $this->render();
    }
    
    public function setError($error) {
        $this->error = $error;
    }
    
    public function getError() {
        return $this->error;
    }
    public function offsetGet($desc) {
        return $this->getError();
    }
}
