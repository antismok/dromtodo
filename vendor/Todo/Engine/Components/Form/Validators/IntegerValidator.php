<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Todo\Engine\Components\Form\Validators;

/**
 * Description of IntegerValidator
 *
 * @author ak
 */
class IntegerValidator 
{
    public function validate($value) 
    {
        return filter_var($value, FILTER_VALIDATE_INT, [ "options" => [ "min_range" => 1 ] ]);
    }
    
    public function getError() 
    {
        return 'Не является числом';
    }
}
