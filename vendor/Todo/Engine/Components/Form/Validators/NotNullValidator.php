<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Todo\Engine\Components\Form\Validators;

use Todo\Engine\Components\Form\Validators\ValidatorInterface;

/**
 * Description of NotNullValidator
 *
 * @author ak
 */
class NotNullValidator implements ValidatorInterface
{
    public function validate($value = null) 
    {
        if (null === $value || $value == "" ) {
            return false;
        }
        return true;
    }
    
    public function getError() {
        return 'Поле не может быть пустым';
    }
}
