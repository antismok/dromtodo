<?php

namespace Todo\Engine\Components\Form\Validators;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ValidatorInterface
 *
 * @author ak
 */
interface ValidatorInterface {
    public function validate($value = null);
    public function getError();
}
