<?php

namespace Todo\Engine\Components;

use Symfony\Component\Yaml\Yaml;
use TodoList\Controllers\TodoListController;
use Todo\Engine\Components\Di\DiContainerInterface;


/**
 * Description of RouterResolver
 *
 * @author ak
 */
class Router
{
    protected $routesFile;
    protected $container;
    protected $ymlParser;
    protected $root = '/';

    public function __construct(DiContainerInterface $container,Yaml $ymlParser, string $routeFile) 
    {
        $this->container = $container;
        $this->routesFile = $routeFile;
        $this->ymlParser = $ymlParser;
    }
    
    public function getRouts() 
    {
        return $this->ymlParser->parse(file_get_contents($this->routesFile));
    }
    
    
    public function resolveController(Request $request)
    {
        $path = $request->server->get("PATH_INFO");
        $routes = $this->getRouts();
        foreach ($routes as $name => $params) {
            if ($params['prefix'] == $this->root.$path) {
                $className = $params['resource']."\Controller\\".$params['controller'].'Controller';
                if (class_exists($className) && method_exists($className,$params['method'])) {
                    $controller = new $className;
                    $controller->setContainer($this->container);
                    $answer = call_user_func_array([$controller, $params['method']], [$request]);
                    unset($controller);
                    return $answer;
                }
            }
        }
        $this->redirect404();
    }
    
    /**
     * Return href from route name
     * @param   string $name    rounte name
     * @return  string|boolean  href
     */
    public function getRoute($name) {
        $routes = $this->getRouts();
        if(isset($routes[$name])) {
            return $routes[$name]['prefix'];
        }
        //TODO бросать исключения вместо лжи
        return false;
    }
    
    //TODO: Это должно быть в респонсе
    public function redirect($href, $status = 302, $headers = [])
    {
        $host = $this->container->get('kernel')->getHost();
        header("Location: ".$host.$href,$status);
    }
    
    /**
     * TODO: Это должно быть в респонсе
     * Redirect to not found page
     */
    public function redirect404() {
         echo "404 Not Found";
        header("HTTP/1.0 404 Not Found");
    }
}
